### STAGE 1: Build ###
FROM node:14.19.3 AS build
WORKDIR /usr/src/app
COPY . .
RUN npm install
RUN npm run build

### STAGE 2: Run ###
FROM nginx:1.20-alpine
ENV FRONTEND_PORT=80
COPY --from=build  /usr/src/app/dist/immobilier /var/www/html
WORKDIR /etc/nginx/conf.d
COPY ./nginx-server/http-server.conf.template ./
RUN rm -f default.conf
CMD ["/bin/sh","-c","envsubst '$$FRONTEND_PORT_HOSTNAME $$FRONTEND_PORT'< http-server.conf.template > http-server.conf && nginx -g 'daemon off;'"]
